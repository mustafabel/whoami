﻿namespace whoami.Calculation
{
    public interface ICalculation
    {
        decimal Calculate();
    }

    public class Addition : ICalculation
    {
        private readonly decimal x;
        private readonly decimal y;

        public Addition(decimal x, decimal y)
        {
            this.x = x;
            this.y = y;
        }

        public decimal Calculate()
        {
            return x + y;
        }
    }

    public class Division : ICalculation
    {
        private readonly decimal x;
        private readonly decimal y;

        public Division(decimal x, decimal y)
        {
            this.x = x;
            this.y = y;
        }

        public decimal Calculate()
        {
            return x / y;
        }
    }

    public class Multiplication : ICalculation
    {
        private readonly decimal x;
        private readonly decimal y;

        public Multiplication(decimal x, decimal y)
        {
            this.x = x;
            this.y = y;
        }

        public decimal Calculate()
        {
            return x * y;
        }
    }

    public class Subtraction : ICalculation
    {
        private readonly decimal x;
        private readonly decimal y;

        public Subtraction(decimal x, decimal y)
        {
            this.x = x;
            this.y = y;
        }

        public decimal Calculate()
        {
            return x - y;
        }
    }
}
