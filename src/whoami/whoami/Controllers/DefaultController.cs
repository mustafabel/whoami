﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace whoami.Controllers
{
    [Route("/")]
    [ApiController]
    [Produces("application/json")]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            var host = System.Net.Dns.GetHostName();
            var osDescription = System.Runtime.InteropServices.RuntimeInformation.OSDescription;
            var remoteIp = Request.HttpContext.Connection.RemoteIpAddress;
            if (remoteIp.IsIPv4MappedToIPv6)
            {
                remoteIp = remoteIp.MapToIPv4();
            }

            var localIp = Request.HttpContext.Connection.LocalIpAddress;
            if (localIp.IsIPv4MappedToIPv6)
            {
                localIp = localIp.MapToIPv4();
            }

            var adapterIPs = new List<object>();

            foreach (var adapter in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                var ipProperties = adapter.GetIPProperties();
                foreach (var ip in ipProperties.UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        adapterIPs.Add(new
                        {
                            Name = adapter.Name,
                            Description = adapter.Description,
                            IP = ip.Address.ToString()
                        });
                    }
                }
            }

            var result = new
            {
                Host = host,
                OSDescription = osDescription,
                RemoteIpAddress = string.Concat(remoteIp, ":", Request.HttpContext.Connection.RemotePort),
                LocalIpAddress = string.Concat(localIp, ":", Request.HttpContext.Connection.LocalPort),
                IPs = adapterIPs,
                Client = new
                {
                    Url = Request.Host.ToUriComponent(),
                    Headers = Request.Headers.ToDictionary(x => x.Key, y => y.Value)
                },
                EnvironmentVariables = System.Environment.GetEnvironmentVariables(),
                CommandLineArgs = System.Environment.GetCommandLineArgs()
            };

            return
                new JsonResult(
                    result,
                    new System.Text.Json.JsonSerializerOptions
                    {
                        WriteIndented = true,
                        Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping
                    });
        }
    }
}
